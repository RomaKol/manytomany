package model;

import java.util.HashSet;
import java.util.Set;

public class Track extends Entity <Integer> {
    private String name;
    private int duration;
    private byte[] data;
    private Album album;

    public Track(){}
    public Track(String name, int duration, byte[] data){
        this.name = name;
        this.duration = duration;
        this.data = data;
        this.album = new Album();
    }
    public Track (Integer id, String name, int duration, byte[] data){
        super(id);
        this.name = name;
        this.duration = duration;
        this.data = data;
        this.album = new Album();
    }
    public Track(String name, int duration, byte[] data, Album album){
        this.name = name;
        this.duration = duration;
        this.data = data;
        this.album = new Album();
    }
    public Track (Integer id, String name, int duration, byte[] data, Album album){
        super(id);
        this.name = name;
        this.duration = duration;
        this.data = data;
        this.album = new Album();
    }

    public String getName() {
        return name;
    }
    public int getDuration() {
        return duration;
    }
    public byte[] getData() {
        return data;
    }
    public Album getAlbum() {
        return album;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public void setData(byte[] data) {
        this.data = data;
    }
    public void setAlbum(Album album) {
        this.album = album;
    }
    
    public String toString(){
//        return super.toString() + " " + this.name;
        return this.name;
    }
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof Track)){
            return false;
        }
        Track track = (Track)object;
        
        return this.id == track.getId()
                && this.name != null && this.name.equals(track.name);
    }
    public int hashCode(){
        int hashCode = 31;
        hashCode = 31 * hashCode + this.id.hashCode();
        hashCode = (this.name != null) ? 31 * hashCode + this.name.hashCode() : hashCode;
        return hashCode;
    }
}
