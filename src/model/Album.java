package model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Album extends Entity <Integer> {
    private String name;
    private String description;
    private Date date;
    private Set<Track> tracks;
    private Set<Performer> performers;
    
    public Album(){
        this.tracks = new HashSet<>();
        this.performers = new HashSet<>();
    }
    public Album(String name, String description, Date date){
        this.name = name;
        this.description = description;
        this.date = date;
        this.tracks = new HashSet<>();
        this.performers = new HashSet<>();
    }
    public Album(Integer id, String name, String description, Date date){
        super(id);
        this.name = name;
        this.description = description;
        this.date = date;
        this.tracks = new HashSet<>();
        this.performers = new HashSet<>();
    }
    public Album(String name, String description, Date date, Set<Track> tracks,
            Set<Performer> performers){
        this.name = name;
        this.description = description;
        this.date = date;
        this.tracks = tracks;
        this.performers = performers;
    }
    public Album(Integer id, String name, String description, Date date,
            Set<Track> tracks, Set<Performer> performers){
        super(id);
        this.name = name;
        this.description = description;
        this.date = date;
        this.tracks = tracks;
        this.performers = performers;
    }
    
    public String getName(){
        return this.name;
    }
    public String getDescription() {
        return description;
    }
    public Date getDate() {
        return date;
    }
    public Set<Track> getTracks() {
        return tracks;
    }
    public Set<Performer> getPerformers() {
        return performers;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }
    public void setPerformers(Set<Performer> performers) {
        this.performers = performers;
    }
    
    public String toString(){
//        return super.toString() + " " + this.name;
        return this.name;
    }
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof Album)){
            return false;
        }
        Album album = (Album)object;
        
        return this.id == album.getId()
                && this.name != null && this.name.equals(album.name);
    }
    public int hashCode(){
        int hashCode = 67;
        hashCode = 67 * hashCode + this.id.hashCode();
        hashCode = (this.name != null) ? 67 * hashCode + this.name.hashCode() : hashCode;
        return hashCode;
    }
    
    public boolean addTrack(Track track){
        return this.tracks.add(track);
    }
    
    public boolean removeTrack(Track track){
        return this.tracks.remove(track);
    }
    
    public boolean addPerformer(Performer performer){
        return this.performers.add(performer);
    }
    
    public boolean removePerformer(Performer performer){
        return this.performers.remove(performer);
    }
    
    public boolean searchTrackName(String trackName){
        for(Track track : tracks){
            if(track.getName().equals(trackName)){
                return true;
            }            
        }
        return false;
    }
    
    public boolean searchPerformerName(String perfName){
        for(Performer performer : performers){
            if(performer.getName().equals(perfName)){
                return true;
            }            
        }
        return false;
    }
}
