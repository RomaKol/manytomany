package model;

public class Entity<K> {
    protected K id;
    
    public Entity(){}
    
    public Entity(K id){
        this.id = id;
    }
    
    public K getId(){
        return this.id;
    }
    public void setId(K id){
        this.id = id;
    }
    
//    public String toString(){
//        return this.id.toString();
//    }
}
