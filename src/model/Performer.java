package model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Performer extends Entity <Long> {
    private String name;
    private byte[] photo;
    private Date date;
    private String description;
    private Set<Album> albums;
    
    public Performer(){
        this.albums = new HashSet<>();
    }
    public Performer(String name, byte[] photo, Date date, String description){
        this.name = name;
        this.photo = photo;
        this.date = date;
        this.description = description;
        this.albums = new HashSet<>();
    }
    public Performer(Long id, String name, byte[] photo, Date date, String description){
        super(id);
        this.name = name;
        this.photo = photo;
        this.date = date;
        this.description = description;
        this.albums = new HashSet<>();
    }
    
    public Performer(String name, byte[] photo, Date date, String description, Set<Album> albums){
        this.name = name;
        this.photo = photo;
        this.date = date;
        this.description = description;
        this.albums = albums;
    }
    public Performer(Long id, String name, byte[] photo, Date date, String description, Set<Album> albums){
        super(id);
        this.name = name;
        this.photo = photo;
        this.date = date;
        this.description = description;
        this.albums = albums;
    }
    
    public String getName(){
        return this.name;
    }
    public byte[] getPhoto(){
        return this.photo;
    }
    public Date getDate(){
        return this.date;
    }
    public Set<Album> getAlbums(){
        return this.albums;
    }
    public String getDescription(){
        return this.description;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setPhoto(byte[] photo){
        this.photo = photo;
    }
    public void setDate(Date date){
        this.date = date;
    }
    public void setAlbums(Set<Album> albums){
        this.albums = albums;
    }
    public void setDescription(String description){
        this.description = description;
    }
    
    public String toString(){
//        return super.toString() + " " + this.name;
        return this.name;
    }
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof Performer)){
            return false;
        }
        Performer performer = (Performer)object;
        
        return this.id == performer.getId()
                && this.name != null && this.name.equals(performer.name);
    }
    public int hashCode(){
        int hashCode = 31;
        hashCode = 31 * hashCode + this.id.hashCode();
        hashCode = (this.name != null) ? 31 * hashCode + this.name.hashCode() : hashCode;
        return hashCode;
    }
    
    public boolean addAlbum(Album album){
        return this.albums.add(album);
    }
    
    public boolean removeAlbum(Album album){
        return this.albums.remove(album);
    }
}
