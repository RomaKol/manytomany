package api.dao;

import java.util.Collection;

public interface Dao<K,E> {
    E create(E entity);
    E update(E entity);
    boolean delete(E entity);
    E findById(K key);
    Collection<E> findAll(int from, int to);
}
