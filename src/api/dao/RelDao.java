package api.dao;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class RelDao<K,E> implements Dao<K,E> {
    protected Connection connection;
    
    public RelDao(Connection connection){
        this.connection = connection;
    }
    
    public void commit(){
        try{
            this.connection.commit();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void rollback(){
        try{
            this.connection.rollback();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
}
