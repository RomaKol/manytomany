package api.dao;

import impl.dao.RelDaoAlbum;
import impl.dao.RelDaoPerformer;
import impl.dao.RelDaoTrack;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import model.Album;
import model.Performer;
import model.Track;
import transferdata.StorageType;
import transferdata.TypeEntityStorage;

public abstract class DaoFactory {
    private static DaoFactory daoFactory;
    
    protected DaoFactory(){}
    
    public static DaoFactory getDaoFactory(){
        if(daoFactory == null){
            try(InputStream in = new BufferedInputStream(new FileInputStream("resources/storage_config.properties"))){
                Properties properties = new Properties();
                properties.load(in);
                StorageType storageType = StorageType.valueOf(properties.getProperty("storage").toUpperCase());
                switch(storageType){
                    case REL:
                        daoFactory = new RelDaoFactory();
                    default:
                        daoFactory = new RelDaoFactory();
                }
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
        return daoFactory;
    }
    
    public abstract <K,E> Dao<K,E> getDao(Class<?> k, Class<?> e);
    
    public abstract void close();
    
    private static class RelDaoFactory extends DaoFactory{
        private Connection connection;
        
        protected RelDaoFactory(){
            connection = getConnection();
        }
        
        private Connection getConnection(){
            if(connection == null){
                try(InputStream in = new BufferedInputStream(new FileInputStream("resources/storage_config.properties"))){
                    Properties properties = new Properties();
                    properties.load(in);
                    connection = DriverManager.getConnection(properties.getProperty("url"),
                            properties.getProperty("login"), properties.getProperty("password"));
                    
                    connection.setAutoCommit(false);
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
                catch(FileNotFoundException e){
                    e.printStackTrace();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
            return connection;
        }

        @Override
        public <K, E> Dao<K, E> getDao(Class<?> k, Class<?> e) {
            try{
                E entity = (E)e.newInstance();
                if(entity instanceof Album){
                    return (Dao<K, E>)(new RelDaoAlbum(getConnection()));
                }
                if(entity instanceof Performer){
                    return (Dao<K, E>)(new RelDaoPerformer(getConnection()));
                }
                if(entity instanceof Track){
                    return (Dao<K, E>)(new RelDaoTrack(getConnection()));
                }
                return null;
            }
            catch(InstantiationException g){
                g.printStackTrace();
            }
            catch(IllegalAccessException g){
                g.printStackTrace();
            }
            return null;
        }

        @Override
        public void close() {
            try{
                connection.close();
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
    }
}
