/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl.dao;

import api.dao.RelDao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import model.Track;

/**
 *
 * @author raketa
 */
public class RelDaoTrack extends RelDao<Integer, Track> {
    
    public RelDaoTrack(Connection connection){
        super(connection);
    }

    @Override
    public Track create(Track entity) {
        String sql = "INSERT INTO tracks (name, duration, data) "
                + "VALUES (?, ?, ?)";
        try(PreparedStatement prepStat = connection.prepareStatement(sql,
                PreparedStatement.RETURN_GENERATED_KEYS)){
            prepStat.setString(1, entity.getName());
            prepStat.setInt(2, entity.getDuration());
            prepStat.setBytes(3, entity.getData());
            
            int numberInsertedRows = prepStat.executeUpdate();
            if(numberInsertedRows == 0){
                return null;
            }
            
            ResultSet resultSet = prepStat.getGeneratedKeys();
            Integer trackId = null;
            if(resultSet.next()){
                trackId = resultSet.getInt(1);
            }
            Track track = new Track(trackId, entity.getName(), entity.getDuration(),
                    entity.getData(), entity.getAlbum());
            
            return track;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Track update(Track entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Track entity) {
        String sql = "DELETE FROM tracks WHERE id = ?";
        try(PreparedStatement prepStat = connection.prepareStatement(sql)){
            prepStat.setInt(1, entity.getId());
            int numberDeletedRows = prepStat.executeUpdate();
            if(numberDeletedRows == 0){
                return false;
            }
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Track findById(Integer key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Track> findAll(int from, int to) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
