/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl.dao;

import api.dao.RelDao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import model.Entity;
import model.Performer;

/**
 *
 * @author raketa
 */
public class RelDaoPerformer extends RelDao<Long, Performer> {
    
    public RelDaoPerformer(Connection connection){
        super(connection);
    }

    @Override
    public Performer create(Performer entity) {
        String sql = "INSERT INTO performers (name, date, photo, description) "
                + "VALUES (?, ?, ?, ?)";
        try(PreparedStatement prepState = connection.prepareStatement(sql,
                PreparedStatement.RETURN_GENERATED_KEYS)){
            prepState.setString(1, entity.getName());
            prepState.setDate(2, new Date(entity.getDate().getTime()));
            prepState.setBytes(3, entity.getPhoto());
            prepState.setString(4, entity.getDescription());
            
            int numberInsertedRows = prepState.executeUpdate();
            if(numberInsertedRows == 0){
                System.out.println("Inserted Rows = 0");
                return null;
            }
            
            ResultSet resultSet = prepState.getGeneratedKeys();
            Long performerId = null;
            if(resultSet.next()){
                performerId = resultSet.getLong(1);
            }
            Performer performer = new Performer(performerId, entity.getName(),
                    entity.getPhoto(), entity.getDate(), entity.getDescription());
            return performer;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Performer update(Performer entity) {
        String sqlDelete = "UPDATE performers SET name = ?, date = ?, photo = ?, description = ? "
                + "WHERE id = ?";
        try(PreparedStatement psDelete = connection.prepareStatement(sqlDelete)){
            // delete old performer
            psDelete.setString(1, entity.getName());
            psDelete.setDate(2, new Date(entity.getDate().getTime()));
            psDelete.setBytes(3, entity.getPhoto());
            psDelete.setString(4, entity.getDescription());
            psDelete.setLong(5, entity.getId());
            
            int numberUpdateRows = psDelete.executeUpdate();
            if(numberUpdateRows == 0){
                return null;
            }
            
            return entity;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(Performer entity) {
        String sql = "DELETE FROM performers WHERE id = ?";
        try(PreparedStatement prepStat = connection.prepareStatement(sql)){
            // delete performer
            prepStat.setLong(1, entity.getId());
            int numbDeltedRows = prepStat.executeUpdate();
            if(numbDeltedRows == 0){
                return false;
            }
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Performer findById(Long key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Performer> findAll(int from, int to) {
        ArrayList<Performer> performers = new ArrayList<>();
        String sql = "SELECT p.id, p.name, p.date, p.photo, p.description "
                + "FROM performers as p LIMIT ?, ?";
        try(PreparedStatement prepStat = connection.prepareStatement(sql)){
            prepStat.setInt(1, from);
            prepStat.setInt(2, to);
            
            ResultSet resultSet = prepStat.executeQuery();
            while(resultSet.next()){
                Long perfId = resultSet.getLong(1);
                String perfName = resultSet.getString(2);
                java.util.Date perfDate = new java.util.Date(resultSet.getDate(3).getTime());
                byte[] perfPhoto = resultSet.getBytes(4);
                String perfDesc = resultSet.getString(5);
                
                Performer performer = new Performer(perfId, perfName, perfPhoto,
                        perfDate, perfDesc);
                performers.add(performer);
            }
            
            return performers;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    
}
