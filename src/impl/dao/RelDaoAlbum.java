package impl.dao;

import api.dao.RelDao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import model.Album;
import model.Performer;
import model.Track;
import util.Criteria;

public class RelDaoAlbum extends RelDao<Integer, Album> {
    
    public RelDaoAlbum(Connection connection){
        super(connection);
    }

    @Override
    public Album create(Album entity) {
        String sql = "INSERT INTO albums (name, description, date) "
                + "VALUES (?, ?, ?)";
        String sql2 = "INSERT INTO albums_performers (id_album, id_performer) "
                + "VALUES (?, ?)";
        String sql3 = "INSERT INTO albums_tracks (id_album, id_track) "
                + "VALUES (?, ?)";
        try(PreparedStatement prepStat = connection.prepareStatement(sql,
                PreparedStatement.RETURN_GENERATED_KEYS);
                PreparedStatement prepStat2 = connection.prepareStatement(sql2);
                PreparedStatement prepStat3 = connection.prepareStatement(sql3)){
            
            prepStat.setString(1, entity.getName());
            prepStat.setString(2, entity.getDescription());
            prepStat.setDate(3, new Date(entity.getDate().getTime()));
            
            int numberInsertedRows = prepStat.executeUpdate();
            if(numberInsertedRows == 0){
                System.out.println("Inserted Rows = 0");
                return null;
            }
            
            ResultSet resultSet = prepStat.getGeneratedKeys();
            Integer albumId = null;
            if(resultSet.next()){
                albumId = resultSet.getInt(1);
            }
            
            // query other tables
            for(Performer performer : entity.getPerformers()){
                prepStat2.setInt(1, albumId);
                prepStat2.setLong(2, performer.getId());
                prepStat2.addBatch();
            }
            int[] numberInsertedRows2 = prepStat2.executeBatch();
            for(int i = 0; i < numberInsertedRows2.length; i ++){
                if(numberInsertedRows2[i] == 0){
                    return null;
                }
            }
            
            for(Track track : entity.getTracks()){
                prepStat3.setInt(1, albumId);
                prepStat3.setInt(2, track.getId());
                prepStat3.addBatch();
            }
            int[] numberInsertedRows3 = prepStat3.executeBatch();
            for(int j = 0; j < numberInsertedRows3.length; j ++){
                if(numberInsertedRows3[j] == 0){
                    return null;
                }
            }
            
            
            Album album = new Album(albumId, entity.getName(), entity.getDescription(),
                    entity.getDate(), entity.getTracks(), entity.getPerformers());
            
            return album;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Album update(Album entity) {
        String sqlGetTracksId = "SELECT at.id_track FROM albums_tracks as at "
                + "WHERE id_album = ?";
        String sqlDelTracks = "DELETE FROM tracks WHERE id = ?";
        String sqlDelAlbum = "DELETE FROM albums WHERE id = ?";
        String sqlInsAlbum = "INSERT INTO albums (id, name, description, date) "
                + "VALUES (?, ?, ?, ?)";
        String sqlInsAlbumsPerformers = "INSERT INTO albums_performers (id_album, id_performer) "
                + "VALUES (?, ?)";
        String sqlInsAlbusTracks = "INSERT INTO albums_tracks (id_album, id_track) "
                + "VALUES (?, ?)";
        try(PreparedStatement psGetTracksId = connection.prepareStatement(sqlGetTracksId);
                PreparedStatement psDelTrack = connection.prepareStatement(sqlDelTracks);
                PreparedStatement psDelAlbum = connection.prepareStatement(sqlDelAlbum);
                PreparedStatement psInsAlbum = connection.prepareStatement(sqlInsAlbum);
                PreparedStatement psInsAlbumsPerformers = connection.prepareStatement(sqlInsAlbumsPerformers);
                PreparedStatement psInsAlbumsTracks = connection.prepareStatement(sqlInsAlbusTracks)){
            // Get id all old track from album
            psGetTracksId.setInt(1, entity.getId());
            ResultSet resultSet = psGetTracksId.executeQuery();
            while(resultSet.next()){
                Integer trackId = resultSet.getInt(1);
                
                // Delte all old tracks from album
                psDelTrack.setInt(1, trackId);
                int resRowDelTrack = psDelTrack.executeUpdate();
                if(resRowDelTrack == 0){
                    return null;
                }
            }
            
            // Delete old album
            psDelAlbum.setInt(1, entity.getId());
            int resRowDelAlbum = psDelAlbum.executeUpdate();
            if(resRowDelAlbum == 0){
                return null;
            }
            
            
            // Insert new album with old id and relations
            psInsAlbum.setInt(1, entity.getId());
            psInsAlbum.setString(2, entity.getName());
            psInsAlbum.setString(3, entity.getDescription());
            psInsAlbum.setDate(4, new Date(entity.getDate().getTime()));
            
            int numberInsertedRows = psInsAlbum.executeUpdate();
            if(numberInsertedRows == 0){
                return null;
            }
            
            // insert new relations
            for(Performer performer : entity.getPerformers()){
                psInsAlbumsPerformers.setInt(1, entity.getId());
                psInsAlbumsPerformers.setLong(2, performer.getId());
                psInsAlbumsPerformers.addBatch();
            }
            int[] numberInsertedRows2 = psInsAlbumsPerformers.executeBatch();
            for(int i = 0; i < numberInsertedRows2.length; i ++){
                if(numberInsertedRows2[i] == 0){
                    return null;
                }
            }
            
            for(Track track : entity.getTracks()){
                psInsAlbumsTracks.setInt(1, entity.getId());
                psInsAlbumsTracks.setInt(2, track.getId());
                psInsAlbumsTracks.addBatch();
            }
            int[] numberInsertedRows3 = psInsAlbumsTracks.executeBatch();
            for(int j = 0; j < numberInsertedRows3.length; j ++){
                if(numberInsertedRows3[j] == 0){
                    return null;
                }
            }
            
            Album album = new Album(entity.getId(), entity.getName(), entity.getDescription(),
                    entity.getDate(), entity.getTracks(), entity.getPerformers());
            
            return album;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(Album entity) {
        String sql = "DELETE FROM albums WHERE id = ?";
        try(PreparedStatement prepStat = connection.prepareStatement(sql)){
            prepStat.setInt(1, entity.getId());
            int numberDeletedRows = prepStat.executeUpdate();
            if(numberDeletedRows == 0){
                return false;
            }
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Album findById(Integer key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Album> findAll(int from, int to) {
        ArrayList<Album> albums = new ArrayList<>();
        String sql = "SELECT a.id, a.name, a.description, a.date "
                + "FROM albums as a LIMIT ?, ?";
        String sql2 = "SELECT p.id, p.name, p.date, p.photo, p.description "
                + "FROM performers as p "
                + "WHERE p.id = ?";
        String sql21 = "SELECT ap.id_performer FROM albums_performers as ap "
                + "WHERE ap.id_album = ?";
        String sql3 = "SELECT t.id, t.name, t.duration, t.data "
                + "FROM tracks as t "
                + "WHERE t.id = ?";
        String sql31 = "SELECT at.id_track FROM albums_tracks as at "
                + "WHERE at.id_album = ?";
        try(PreparedStatement prepStat = connection.prepareStatement(sql); 
                PreparedStatement prepStat2 = connection.prepareStatement(sql2);
                PreparedStatement prepStat21 = connection.prepareStatement(sql21);
                PreparedStatement prepStat3 = connection.prepareStatement(sql3);
                PreparedStatement prepStat31 = connection.prepareStatement(sql31)){
            prepStat.setInt(1, from);
            prepStat.setInt(2, to);
            
            ResultSet resultSet = prepStat.executeQuery();
            while(resultSet.next()){
                Integer albumId = resultSet.getInt(1);
                String albumName = resultSet.getString(2);
                String albumDescription = resultSet.getString(3);
                java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                
                Album album = new Album(albumId, albumName, albumDescription, albumDate);
                
                prepStat21.setInt(1, albumId);
                ResultSet resultSet21 = prepStat21.executeQuery();
                while(resultSet21.next()){
                    prepStat2.setInt(1, resultSet21.getInt(1));
                    ResultSet resultSet2 = prepStat2.executeQuery();
                    while(resultSet2.next()){
                        Long perfId = resultSet2.getLong(1);
                        String perfName = resultSet2.getString(2);
                        java.util.Date perfDate = new java.util.Date(resultSet2.getDate(3).getTime());
                        byte[] perfPhoto = resultSet2.getBytes(4);
                        String perfDescription = resultSet2.getString(5);

                        Performer performer = new Performer(perfId, perfName, perfPhoto,
                                perfDate, perfDescription);
                        performer.addAlbum(album);
                        album.addPerformer(performer);
                    }
                }
                
                prepStat31.setInt(1, albumId);
                ResultSet resultSet31 = prepStat31.executeQuery();
                while(resultSet31.next()){
                    prepStat3.setInt(1, resultSet31.getInt(1));
                    ResultSet resultSet3 = prepStat3.executeQuery();
                    while(resultSet3.next()){
                        Integer trackId = resultSet3.getInt(1);
                        String trackName = resultSet3.getString(2);
                        Integer trackDuration = resultSet3.getInt(3);
                        byte[] trackData = resultSet3.getBytes(4);

                        Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                        album.addTrack(track);
                    }
                }
                
                albums.add(album);
            }
            
            return albums;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    
    public Collection<Album> search(Criteria criteria, int from, int to){
        HashSet<Album> albums = new HashSet<>();
        // search by album name
        if(criteria.getNameAlbum() != null && criteria.getNameTrack() == null
                && criteria.getNamePerforer() == null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer "
                    + "WHERE a.name = ?";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                prepStat.setString(1, criteria.getNameAlbum());
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                
                return albums;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by track name
        else if(criteria.getNameAlbum() == null && criteria.getNameTrack() != null
                && criteria.getNamePerforer() == null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchTrackName(criteria.getNameTrack()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by performer name
        else if(criteria.getNameAlbum() == null && criteria.getNameTrack() == null
                && criteria.getNamePerforer() != null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    System.out.println("performer 1");
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchPerformerName(criteria.getNamePerforer()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by album name and track name
        else if(criteria.getNameAlbum() != null && criteria.getNameTrack() != null
                && criteria.getNamePerforer() == null){            
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer "
                    + "WHERE a.name = ?";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                prepStat.setString(1, criteria.getNameAlbum());
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchTrackName(criteria.getNameTrack()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by album name and performer name
        else if(criteria.getNameAlbum() != null && criteria.getNameTrack() == null
                && criteria.getNamePerforer() != null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer "
                    + "WHERE a.name = ?";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                prepStat.setString(1, criteria.getNameAlbum());
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchPerformerName(criteria.getNamePerforer()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by performer name and track name
        else if(criteria.getNameAlbum() == null && criteria.getNameTrack() != null
                && criteria.getNamePerforer() != null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchTrackName(criteria.getNameTrack()))
                        .filter(t -> t.searchPerformerName(criteria.getNamePerforer()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // search by performer name and track name
        else if(criteria.getNameAlbum() != null && criteria.getNameTrack() != null
                && criteria.getNamePerforer() != null){
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer "
                    + "WHERE a.name = ?";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                prepStat.setString(1, criteria.getNameAlbum());
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                Stream<Album> streamAlbum = albums.stream();
                return streamAlbum.filter(t -> t.searchTrackName(criteria.getNameTrack()))
                        .filter(t -> t.searchPerformerName(criteria.getNamePerforer()))
                        .collect(Collectors.toSet());
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        // no criteria data
        else{
            String sql = "SELECT a.id, a.name, a.description, a.date, at.id_track, "
                    + "t.name, t.duration, t.data, ap.id_performer, p.name, p.date, "
                    + "p.photo, p.description FROM music.albums as a\n" +
                "LEFT JOIN music.albums_tracks as at ON a.id = at.id_album\n" +
                "LEFT JOIN music.tracks as t ON t.id = at.id_track\n" +
                "LEFT JOIN music.albums_performers as ap ON a.id = ap.id_album\n" +
                "LEFT JOIN music.performers as p ON p.id = ap.id_performer";
            try(PreparedStatement prepStat = connection.prepareStatement(sql)){
                ResultSet resultSet = prepStat.executeQuery();
                while(resultSet.next()){
                    Integer albumId = resultSet.getInt(1);
                    String albumName = resultSet.getString(2);
                    String albumDesc = resultSet.getString(3);
                    java.util.Date albumDate = new java.util.Date(resultSet.getDate(4).getTime());
                    Integer trackId = resultSet.getInt(5);
                    String trackName = resultSet.getString(6);
                    Integer trackDuration = resultSet.getInt(7);
                    byte[] trackData = resultSet.getBytes(8);
                    Long perfId = resultSet.getLong(9);
                    String perfName = resultSet.getString(10);
                    java.util.Date perfDate = resultSet.getDate(11);
                    byte[] perfPhoto = resultSet.getBytes(12);
                    String perfDescription = resultSet.getString(13);
                    
                    Album album = new Album(albumId, albumName, albumDesc, albumDate);
                    albums.add(album);
                    Track track = new Track(trackId, trackName, trackDuration, trackData, album);
                    album.addTrack(track);
                    Performer performer = new Performer(perfId, perfName, perfPhoto, perfDate, perfDescription);
                    performer.addAlbum(album);
                    album.addPerformer(performer);
                }
                
                return albums;
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
        return null;
    }
    
}
