package impl.services;

import api.dao.Dao;
import api.dao.DaoFactory;
import api.dao.RelDao;
import api.services.EntityService;
import impl.dao.RelDaoTrack;
import java.util.Collection;
import model.Track;

public class TrackService implements EntityService<Integer, Track>{
    @Override
    public Track create(Track entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Track> dao = daoFactory.getDao(Integer.class, Track.class);
        RelDao<Integer, Track> relDao = (RelDao<Integer, Track>)dao;
        RelDaoTrack relDaoTrack = (RelDaoTrack)relDao;
        
        Track resTrack = (Track)(dao.create(entity));
        if(resTrack == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resTrack;
    }

    @Override
    public Track update(Track entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Track> dao = daoFactory.getDao(Integer.class, Track.class);
        RelDao<Integer, Track> relDao = (RelDao<Integer, Track>)dao;
        RelDaoTrack relDaoTrack = (RelDaoTrack)relDao;
        
        Track resTrack = (Track)(dao.update(entity));
        if(resTrack == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resTrack;
    }

    @Override
    public boolean delete(Track entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Track> dao = daoFactory.getDao(Integer.class, Track.class);
        RelDao<Integer, Track> relDao = (RelDao<Integer, Track>)dao;
        RelDaoTrack relDaoTrack = (RelDaoTrack)relDao;
        
        boolean result = dao.delete(entity);
        if(!result){
            relDao.rollback();
            return false;
        }
        relDao.commit();
        return true;
    }

    @Override
    public Track findById(Integer key){
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Track> dao = daoFactory.getDao(Integer.class, Track.class);
        RelDao<Integer, Track> relDao = (RelDao<Integer, Track>)dao;
        RelDaoTrack relDaoTrack = (RelDaoTrack)relDao;
        
        Track resTrack = (Track)(dao.findById(key));
        if(resTrack == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resTrack;
    }
    
    @Override
    public Collection<Track> findAll(int from, int to) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Track> dao = daoFactory.getDao(Integer.class, Track.class);
        RelDao<Integer, Track> relDao = (RelDao<Integer, Track>)dao;
        RelDaoTrack relDaoTrack = (RelDaoTrack)relDao;
        
        Collection<Track> resCollection = dao.findAll(from, to);
        return resCollection;
    }
}
