package impl.services;

import api.dao.Dao;
import api.dao.DaoFactory;
import api.dao.RelDao;
import api.services.EntityService;
import impl.dao.RelDaoPerformer;
import java.util.Collection;
import model.Performer;

public class PerformerService implements EntityService<Long, Performer>{

    @Override
    public Performer create(Performer entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Performer> dao = daoFactory.getDao(Long.class, Performer.class);
        RelDao<Long, Performer> relDao = (RelDao<Long, Performer>)dao;
        RelDaoPerformer relDaoPerformer = (RelDaoPerformer)relDao;
        
        Performer resPerformer = (Performer)(dao.create(entity));
        if(resPerformer == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resPerformer;
    }

    @Override
    public Performer update(Performer entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Performer> dao = daoFactory.getDao(Long.class, Performer.class);
        RelDao<Long, Performer> relDao = (RelDao<Long, Performer>)dao;
        RelDaoPerformer relDaoPerformer = (RelDaoPerformer)relDao;
        
        Performer resPerformer = (Performer)(dao.update(entity));
        if(resPerformer == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resPerformer;
    }

    @Override
    public boolean delete(Performer entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Performer> dao = daoFactory.getDao(Long.class, Performer.class);
        RelDao<Long, Performer> relDao = (RelDao<Long, Performer>)dao;
        RelDaoPerformer relDaoPerformer = (RelDaoPerformer)relDao;
        
        boolean result = dao.delete(entity);
        if(!result){
            relDao.rollback();
            return false;
        }
        relDao.commit();
        return true;
    }

    @Override
    public Performer findById(Long key) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Performer> dao = daoFactory.getDao(Long.class, Performer.class);
        RelDao<Long, Performer> relDao = (RelDao<Long, Performer>)dao;
        RelDaoPerformer relDaoPerformer = (RelDaoPerformer)relDao;
        
        Performer resPerformer = dao.findById(key);
        return resPerformer;
    }

    @Override
    public Collection<Performer> findAll(int from, int to) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Long, Performer> dao = daoFactory.getDao(Long.class, Performer.class);
        RelDao<Long, Performer> relDao = (RelDao<Long, Performer>)dao;
        RelDaoPerformer relDaoPerformer = (RelDaoPerformer)relDao;
        
        Collection<Performer> resCollection = dao.findAll(from, to);
        return resCollection;
    }
    
}
