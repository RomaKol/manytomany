package impl.services;

import api.dao.Dao;
import api.dao.DaoFactory;
import api.dao.RelDao;
import api.services.EntityService;
import impl.dao.RelDaoAlbum;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import model.Album;
import model.Track;
import transferdata.TransferObjectTrackList;
import util.Criteria;

public class AlbumService implements EntityService<Integer, Album> {      
    @Override
    public Album create(Album entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Dao<Integer,Track> daoTrack = daoFactory.getDao(Integer.class, Track.class);
        
        Set<Track> newTraks = new HashSet<>();
        for(Track track : entity.getTracks()){
            Track newTrack = (Track)(daoTrack.create(track));
            newTraks.add(track);
        }
        entity.setTracks(newTraks);
        
        Album resAlbum = (Album)(dao.create(entity));
        if(resAlbum == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resAlbum;
    }
    
    public Album create(Album entity, TransferObjectTrackList trackList) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Dao<Integer,Track> daoTrack = daoFactory.getDao(Integer.class, Track.class);
        
        Set<Track> newTraks = new HashSet<>();
        for(Object object : trackList.getTracks()){
            Track track = (Track)object;
            Track newTrack = (Track)(daoTrack.create(track));
            newTraks.add(newTrack);
        }
        entity.setTracks(newTraks);
        
        Album resAlbum = (Album)(dao.create(entity));
        if(resAlbum == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resAlbum;
    }

    @Override
    public Album update(Album entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Album resAlbum = (Album)(dao.update(entity));
        if(resAlbum == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resAlbum;
    }
    
    public Album update(Album entity, TransferObjectTrackList trackList){
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Dao<Integer,Track> daoTrack = daoFactory.getDao(Integer.class, Track.class);
        
        Set<Track> newTraks = new HashSet<>();
        for(Object object : trackList.getTracks()){
            Track track = (Track)object;
            Track newTrack = (Track)(daoTrack.create(track));
            newTraks.add(newTrack);
        }
        entity.setTracks(newTraks);
        
        Album resAlbum = (Album)(dao.update(entity));
        if(resAlbum == null){
            relDao.rollback();
            return null;
        }
        relDao.commit();
        return resAlbum;
    }

    @Override
    public boolean delete(Album entity) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        boolean result = dao.delete(entity);
        if(!result){
            relDao.rollback();
            return result;
        }
        relDao.commit();
        return result;
    }

    @Override
    public Album findById(Integer key) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Album resAlbum = (Album)dao.findById(key);
        return resAlbum;
    }

    @Override
    public Collection<Album> findAll(int from, int to) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer,Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Collection<Album> resCollection = dao.findAll(from, to);
        return resCollection;
    }
    
    public Collection<Album> search(Criteria criteria, int from, int to) {
        DaoFactory daoFactory = DaoFactory.getDaoFactory();
        Dao<Integer, Album> dao = daoFactory.getDao(Integer.class, Album.class);
        RelDao<Integer, Album> relDao = (RelDao<Integer, Album>)dao;
        RelDaoAlbum relDaoAlbum = (RelDaoAlbum)relDao;
        
        Collection<Album> resCollection = relDaoAlbum.search(criteria, from, to);
        return resCollection;
    }
}
